import React from "react";
import TD from "./pages/td";
import Comp from "./pages/component";
import Intro from "./pages/intro";
import Props from './pages/props';
import State from './pages/state';
import Lifecycle from './pages/lifecycle';

import { Switch, Route, Link, BrowserRouter as Router } from "react-router-dom";

import  "./App.css";

const App = () => {
  return (
    <div className="app-container">
      <Router>
        <nav className="navigation-bar">
          <ul>
          <li>
              <Link to="/">1. Intro</Link>
            </li>
            <li>
              <Link to="/comp">2. Composants</Link>
            </li>
            <li>
              <Link to="/props">3. Props</Link>
            </li>
            <li>
              <Link to="/state">3. State</Link>
            </li>
            <li>
              <Link to="/lifecycle">4. Lifecycle</Link>
            </li>
            <li>
              <Link to="/td">5. TD</Link>
            </li>
          </ul>
        </nav>
        <Switch>
          <Route exact path="/">
            <Intro />
          </Route>
          <Route exact path="/comp">
            <Comp />
          </Route>
          <Route exact path="/props">
            <Props />
          </Route>
          <Route exact path="/state">
            <State />
          </Route>
          <Route exact path="/lifecycle">
            <Lifecycle />
          </Route>
          <Route exact path="/td">
            <TD />
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
