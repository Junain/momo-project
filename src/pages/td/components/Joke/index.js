import React from 'react';
import './styles.css'

const i18n = {
  'en': 'Anglais',
};

const Footer = ({lang, type}) => {
  return <span className="footer-joke">
    <p>{i18n[lang]}</p>
    <p>{type}</p>
  </span>
}

const Header = ({children}) => {
  return <h1>{children}</h1>
}



const Joke = ({data}) => {
  const {category, joke, lang, type} = data;
  return (
    <div className="container-joke">
      <Header>{category}</Header>
      <p>
        {joke}
      </p>
      <Footer lang={lang} type={type} />
    </div>
  )
}

export default Joke;