import React from 'react';
import Joke from '../Joke';

const JokeList = ({jokes}) => {
  return (
  <div>
   {jokes.map((data) => 
    <Joke data={data} />
   )}
   </div>
  )
}

export default JokeList;