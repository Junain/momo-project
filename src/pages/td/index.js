import React, {useEffect, useState} from 'react';
import {get} from 'axios';
import JokeList from './components/JokeList';

const App = () => {
  const [jokes, setJokes] = useState([]);

  useEffect(() => {
    const getJokes = async () => {
      const {data: {jokes: data}} = await get('https://v2.jokeapi.dev/joke/Any?type=single&amount=10')
      
      setJokes(data);
    }
    getJokes();
  }, [])

  if (jokes.length === 0) {
    return <p>Loading...</p>
  
  }
  return <JokeList jokes={jokes} />

}

export default App;
