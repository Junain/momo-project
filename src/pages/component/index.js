import React, {Component} from 'react';

const Comp1 = () => 
  <p>Je suis un composant à base de fonction (FC), je contiens un élement de p</p>
;

class Comp2 extends Component {
  render () {
    return <p>Je suis un composant à base de Classe</p>
  }
}

const Comp3 = () => {
  return (
    <div>
     <p>Je suis un nouveau composant issu d'une composition</p>
    <Comp1 />
    <Comp2 />
    </div>
  )
}

export default Comp3;