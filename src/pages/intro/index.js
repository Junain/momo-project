import React from 'react';

const Intro = () => {
  return (
    <ul>
      <li>Qu'est-ce que React (JSX, create-react-app...) ?</li>
      <li>Un composant, c'est quoi ?</li>
      <li>Propriétés d'un composant (children...)</li>
      <li>Etat d'un composant</li>
      <li>Cycle de vie et hooks </li>
      <li>Let's do it ! </li>
    </ul>
  )
}

export default Intro;