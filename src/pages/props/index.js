import React, {Component} from 'react';
import styles from './styles.module.css';
import './styles.css';

const Comp1 = ({color}) => 
  <p className={`${styles.component} ${color}`}>Je suis un composant à base de fonction (FC)</p>
;

class Comp2 extends Component {
  render () {
    const { color } = this.props;
    return <p className={`${styles.component} ${color}`}>Je suis un composant à base de Classe</p>
  }
}

const Comp3 = () => {
  return (
    <div>
     <p>Je suis un nouveau composant issu d'une composition</p>
    <Comp1 color="red" />
    <Comp2 color="green" />
    </div>
  )
}

export default Comp3;