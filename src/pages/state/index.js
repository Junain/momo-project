import React, {Component, useState} from 'react';
import styles from '../props/styles.module.css';
import '../props/styles.css';

const Comp1 = ({color, text}) => 
  <p className={`${styles.component} ${color}`}>{text}</p>
;

Comp1.defaultProps = {
  text: 'Je suis toujours un composant à base de fonction'
}

class Comp2 extends Component {
  render () {
    const { color, count, children } = this.props;
    return (
    <div>
    <p className={`${styles.component} ${color}`}>Tu as cliqué {count} fois</p>
    {children}
    </div>
    );

  }
}

const Comp3 = () => {
  const [count, setCount] = useState(0);

  const handleClick = () => {
    setCount(count + 1);
  }

  console.count('render');

  return (
    <div>
     <p>Je suis un nouveau composant issu d'une composition</p>
    <Comp1 color="red" />
    <Comp2 count={count} color="green">
      et c'est déjà pas mal...
    </Comp2>
    <button onClick={handleClick}>Click me </button>
    </div>
  )
}

export default Comp3;