import React, {Component, useState, useEffect} from 'react';
import styles from '../props/styles.module.css';
import '../props/styles.css';

class Comp2 extends Component {
  render () {
    const { color, count } = this.props;
    return (
    <div>
    <p className={`${styles.component} ${color}`}>Tu as cliqué {count} fois</p>
    </div>
    );

  }
}

const Comp3 = () => {

  const [count, setCount] = useState(0);

  useEffect(() => {
    console.log('Le composant a été monté');
    return () => {
      console.log('Le composant a disparu...');
    }
  }, [])

  useEffect(() => {
    console.log('Le composant a été mise à jour');
  }, [count])

  const handleClick = () => {
    setCount(count + 1);
  }

  return (
    <div>
    <Comp2 count={count} color="green" />
    <button onClick={handleClick}>Click me </button>
    </div>
  )
}

export default Comp3;